import {Component, Input} from '@angular/core';
import {Role} from "../model/role";

@Component({
  selector: 'app-user-role-badge',
  templateUrl: './user-role-badge.component.html',
  styleUrls: ['./user-role-badge.component.scss']
})
export class UserRoleBadgeComponent {

  @Input()
  public roles!: Role[]

  public get image(): string {
    if(this.roles.includes(Role.ROOT) || this.roles.includes(Role.ADMIN)) {
      return '/assets/icons/role-admin-icon.svg';
    }

    return '/assets/icons/role-user-icon.svg';
  }

}
