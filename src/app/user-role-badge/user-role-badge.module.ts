import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoleBadgeComponent } from './user-role-badge.component';



@NgModule({
  declarations: [
    UserRoleBadgeComponent
  ],
  exports: [
    UserRoleBadgeComponent
  ],
  imports: [
    CommonModule
  ]
})
export class UserRoleBadgeModule { }
