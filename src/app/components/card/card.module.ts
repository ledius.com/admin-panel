import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { CardTextComponent } from './card-text/card-text.component';



@NgModule({
  declarations: [
    CardComponent,
    CardTextComponent
  ],
  exports: [
    CardComponent,
    CardTextComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CardModule { }
