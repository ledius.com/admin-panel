import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent {

  @Input()
  public checked: boolean = false;

  @Output()
  public checkedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  public onChange(event: Event) {
    this.checkedChange.next((event.target as HTMLInputElement).checked);
  }

}
