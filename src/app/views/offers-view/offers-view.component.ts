import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {OffersService} from "../../services/offers.service";
import {Offer} from "../../model/offer/offer";

@Component({
  selector: 'app-offers-view',
  templateUrl: './offers-view.component.html',
  styleUrls: ['./offers-view.component.scss']
})
export class OffersViewComponent {

  public offers: Offer[] = [];

  constructor(
    private readonly router: Router,
    private readonly offersService: OffersService,
  ) {
    this.offersService.$offers.subscribe(offers => this.offers = offers);
  }

  public async onOfferForward(offer: {id: string}): Promise<void> {
    await this.router.navigateByUrl(`/offers/${offer.id}/details`)
  }
}
