import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersViewComponent } from './offers-view.component';
import {OffersListModule} from "../../offers-list/offers-list.module";



@NgModule({
  declarations: [
    OffersViewComponent
  ],
    imports: [
        CommonModule,
        OffersListModule
    ]
})
export class OffersViewModule { }
