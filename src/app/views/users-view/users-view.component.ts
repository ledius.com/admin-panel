import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../../model/exchange/user";
import {UsersService} from "../../services/exchange/users.service";

@Component({
  selector: 'app-users-view',
  templateUrl: './users-view.component.html',
  styleUrls: ['./users-view.component.scss']
})
export class UsersViewComponent  {

  public users: User[] = [];

  constructor(
    private readonly router: Router,
    private readonly usersService: UsersService
  ) {
    this.usersService.$users.subscribe(users => this.users = users);
  }

  public async onForward(user: User): Promise<void> {
    await this.router.navigateByUrl(`/users/${user.userId}/details`);
  }

}
