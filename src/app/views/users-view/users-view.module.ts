import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersViewComponent } from './users-view.component';
import {UsersListModule} from "../../users-list/users-list.module";



@NgModule({
  declarations: [
    UsersViewComponent
  ],
    imports: [
        CommonModule,
        UsersListModule
    ]
})
export class UsersViewModule { }
