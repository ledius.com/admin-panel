import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BalancesViewComponent } from './balances-view.component';
import {CardModule} from "../../components/card/card.module";
import {AccountBalanceListModule} from "../../account-balance-list/account-balance-list.module";



@NgModule({
  declarations: [
    BalancesViewComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    AccountBalanceListModule
  ]
})
export class BalancesViewModule { }
