import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BalancesViewComponent } from './balances-view.component';

describe('BalancesViewComponent', () => {
  let component: BalancesViewComponent;
  let fixture: ComponentFixture<BalancesViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BalancesViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BalancesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
