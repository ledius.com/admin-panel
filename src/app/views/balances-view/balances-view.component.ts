import { Component, OnInit } from '@angular/core';
import {AccountWithBalances} from "../../account-balance-list/account-balance-list.component";
import {AccountService} from "../../services/account.service";

@Component({
  selector: 'app-balances-view',
  templateUrl: './balances-view.component.html',
  styleUrls: ['./balances-view.component.scss']
})
export class BalancesViewComponent{

  public accounts: AccountWithBalances[] = [];


  constructor(
    private readonly accountService: AccountService,
  ) {
    this.accountService.$systemBalances.subscribe(accounts => {
      this.accounts = accounts.map(account => ({
        name: 'Dispenser',
        ...account,
      }))
    })
  }

}
