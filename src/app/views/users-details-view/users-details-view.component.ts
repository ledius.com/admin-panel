import { Component, OnInit } from '@angular/core';
import {UsersService} from "../../services/exchange/users.service";
import {User} from "../../model/exchange/user";
import {AuthService} from "../../services/auth.service";
import {Role} from "../../model/role";

@Component({
  selector: 'app-users-details-view',
  templateUrl: './users-details-view.component.html',
  styleUrls: ['./users-details-view.component.scss']
})
export class UsersDetailsViewComponent {

  public user!: User;

  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {
    this.usersService.$user.subscribe(user => {
      if(user) {
        this.user = user
      }
    });
  }

  public async onUpdateRoles(roles: Role[]): Promise<void> {
    await this.authService.updateRoles(this.user.userId, roles);
    await this.usersService.fetchUserById(this.user.userId);
  }

}
