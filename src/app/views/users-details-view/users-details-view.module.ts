import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersDetailsViewComponent } from './users-details-view.component';
import {UserDetailsModule} from "../../user-details/user-details.module";



@NgModule({
  declarations: [
    UsersDetailsViewComponent
  ],
  imports: [
    CommonModule,
    UserDetailsModule
  ]
})
export class UsersDetailsViewModule { }
