import { Component, OnInit } from '@angular/core';
import {Course} from "../../model/learn/course";
import {CoursesService} from "../../services/learn/courses.service";

@Component({
  selector: 'app-courses-view',
  templateUrl: './courses-view.component.html',
  styleUrls: ['./courses-view.component.scss']
})
export class CoursesViewComponent {

  public courses: Course[] = [];

  constructor(
    private readonly coursesService: CoursesService,
  ) {
    this.coursesService.$courses.subscribe(courses => this.courses = courses);
  }
}
