import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesViewComponent } from './courses-view.component';
import {CourseListModule} from "../../course-list/course-list.module";



@NgModule({
  declarations: [
    CoursesViewComponent
  ],
  imports: [
    CommonModule,
    CourseListModule
  ]
})
export class CoursesViewModule { }
