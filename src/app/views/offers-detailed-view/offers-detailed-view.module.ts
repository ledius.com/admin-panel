import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersDetailedViewComponent } from './offers-detailed-view.component';
import {OffersDetailsModule} from "../../offers-details/offers-details.module";



@NgModule({
  declarations: [
    OffersDetailedViewComponent
  ],
    imports: [
        CommonModule,
        OffersDetailsModule
    ]
})
export class OffersDetailedViewModule { }
