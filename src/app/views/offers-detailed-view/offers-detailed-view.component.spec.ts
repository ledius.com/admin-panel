import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersDetailedViewComponent } from './offers-detailed-view.component';

describe('OffersDetailedViewComponent', () => {
  let component: OffersDetailedViewComponent;
  let fixture: ComponentFixture<OffersDetailedViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OffersDetailedViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OffersDetailedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
