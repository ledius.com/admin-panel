import { Component } from '@angular/core';
import {Offer} from "../../model/offer/offer";
import {OffersService} from "../../services/offers.service";

@Component({
  selector: 'app-offers-detailed-view',
  templateUrl: './offers-detailed-view.component.html',
  styleUrls: ['./offers-detailed-view.component.scss']
})
export class OffersDetailedViewComponent {

  public offer!: Offer;

  constructor(
    private readonly offersService: OffersService
  ) {
    this.offersService.$offer.subscribe(offer =>  {
      if(offer) {
        this.offer = offer;
      }
    });
  }

}
