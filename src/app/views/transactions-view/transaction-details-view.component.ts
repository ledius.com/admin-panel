import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-transaction-details-view',
  templateUrl: './transaction-details-view.component.html',
  styleUrls: ['./transaction-details-view.component.scss']
})
export class TransactionDetailsViewComponent implements OnInit {

  public transactionId: string;

  constructor(
    private readonly route: ActivatedRoute,
  ) {
    this.transactionId = route.snapshot.params['transactionId'];
  }

  ngOnInit(): void {
  }

}
