import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-transactions-view',
  templateUrl: './transactions-view.component.html',
  styleUrls: ['./transactions-view.component.scss']
})
export class TransactionsViewComponent implements OnInit {

  constructor(
    private readonly router: Router
  ) { }

  public async onOpenTransaction(transaction: {id: string}): Promise<void> {
    await this.router.navigateByUrl(`/transactions/${transaction.id}}/details`);
  }

  ngOnInit(): void {
  }

}
