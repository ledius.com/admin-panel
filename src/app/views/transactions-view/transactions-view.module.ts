import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsViewComponent } from './transactions-view.component';
import {TransactionListModule} from "../../transaction-list/transaction-list.module";
import { TransactionDetailsViewComponent } from './transaction-details-view.component';
import {TransactionDetailsModule} from "../../transaction-details/transaction-details.module";



@NgModule({
  declarations: [
    TransactionsViewComponent,
    TransactionDetailsViewComponent
  ],
  imports: [
    CommonModule,
    TransactionListModule,
    TransactionDetailsModule
  ]
})
export class TransactionsViewModule { }
