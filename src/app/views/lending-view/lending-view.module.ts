import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LendingViewComponent } from './lending-view.component';
import {LendingListModule} from "../../lending-list/lending-list.module";



@NgModule({
  declarations: [
    LendingViewComponent
  ],
  imports: [
    CommonModule,
    LendingListModule
  ]
})
export class LendingViewModule { }
