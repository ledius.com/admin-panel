import { Component, OnInit } from '@angular/core';
import {LendingService} from "../../services/lending.service";
import {Lending} from "../../model/lending/lending";

@Component({
  selector: 'app-lending-view',
  templateUrl: './lending-view.component.html',
  styleUrls: ['./lending-view.component.scss']
})
export class LendingViewComponent {

  public lendingList: Lending[] = [];

  constructor(
    private readonly lendingService: LendingService
  ) {
    this.lendingService.$lendingList.subscribe(list => this.lendingList = list);
  }
}
