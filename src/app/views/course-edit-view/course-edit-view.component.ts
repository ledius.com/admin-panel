import { Component } from '@angular/core';
import {Course} from "../../model/learn/course";
import {CoursesService} from "../../services/learn/courses.service";
import {UpdateCourseEvent} from "../../course-edit-form/course-edit-form.component";

@Component({
  selector: 'app-course-edit-view',
  templateUrl: './course-edit-view.component.html',
  styleUrls: ['./course-edit-view.component.scss']
})
export class CourseEditViewComponent {

  public course!: Course;

  constructor(
    private readonly courseService: CoursesService
  ) {
    this.courseService.$course.subscribe(course => {
      if(course) {
        this.course = course;
      }
    })
  }

  public async onUpdate(data: UpdateCourseEvent): Promise<void> {
    await this.courseService.update(this.course.id, data);
  }
}
