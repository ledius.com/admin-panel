import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseEditViewComponent } from './course-edit-view.component';
import {CourseEditFormModule} from "../../course-edit-form/course-edit-form.module";



@NgModule({
  declarations: [
    CourseEditViewComponent
  ],
  imports: [
    CommonModule,
    CourseEditFormModule
  ]
})
export class CourseEditViewModule { }
