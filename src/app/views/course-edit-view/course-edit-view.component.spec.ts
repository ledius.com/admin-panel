import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseEditViewComponent } from './course-edit-view.component';

describe('CourseEditViewComponent', () => {
  let component: CourseEditViewComponent;
  let fixture: ComponentFixture<CourseEditViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseEditViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseEditViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
