import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleManageComponent } from './role-manage.component';
import {SwitchModule} from "../components/switch/switch.module";



@NgModule({
  declarations: [
    RoleManageComponent
  ],
  exports: [
    RoleManageComponent
  ],
  imports: [
    CommonModule,
    SwitchModule
  ]
})
export class RoleManageModule { }
