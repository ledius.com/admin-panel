import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Role} from "../model/role";

@Component({
  selector: 'app-role-manage',
  templateUrl: './role-manage.component.html',
  styleUrls: ['./role-manage.component.scss']
})
export class RoleManageComponent {

  public allRoles: Role[] = [
    Role.ROOT, Role.ADMIN, Role.CUSTOMER
  ];

  @Input()
  public roles!: Role[];

  @Output()
  public updatedRoles: EventEmitter<Role[]> = new EventEmitter<Role[]>()

  public getRoleTitle(role: Role) {
    return {
      [Role.ROOT]: 'Супер-пользователь',
      [Role.ADMIN]: 'Администратор',
      [Role.CUSTOMER]: 'Пользователь'
    }[role] || role;
  }

  public onUpdateRoles(role: Role, status: boolean) {
    if(status) {
      this.roles.push(role);
    }
    if(!status) {
      this.roles = this.roles.filter(item => item !== role);
    }
    this.updatedRoles.emit(this.roles);
  }
}
