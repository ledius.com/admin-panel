export class Account {
  public readonly id!: string;
  public readonly userId!: string;
  public readonly address!: string;
}
