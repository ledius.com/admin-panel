export enum Role {
  ROOT = "ROOT",
  ADMIN = "ADMIN",
  CUSTOMER = "CUSTOMER"
}
