import {FormattedAmount} from "../formatted-amount";

export class Course {
  readonly id!: string;
  readonly content!: {
    readonly name: string;
    readonly icon: string;
  };
  readonly price!: string;
  readonly isPublished!: boolean;
  readonly prices!: {
    readonly lds: string;
    readonly rub: string;
  };

  readonly chips!: {
    readonly icon: string;
    readonly text: string;
  }[];

  readonly tag!: {
    readonly id: string;
    readonly name: string;
    readonly slug: string;
  };

  public getLdsPrice(): string {
    return new FormattedAmount(BigInt(this.prices.lds)).format(2);
  }

  public getRubPrice(): string {
    return String(
      BigInt(this.prices.rub)
    );
  }
}
