export class BankDetails {
  public readonly id!: string;
  public readonly userId!: string;
  public readonly card!: string;
  public readonly name!: { first: string, last: string, patronymic: string };
  public readonly bank!: string;

  public get fullName(): string {
    return `${this.name.last} ${this.name.first} ${this.name.patronymic}`;
  }
}
