import {Account} from "../account/account";
import {Balance} from "../account/balance";
import {Type} from "class-transformer";
import {FormattedAmount} from "../formatted-amount";
import {Role} from "../role";

export class User {
  public readonly userId!: string;
  public readonly email!: string;
  public readonly name!: string;
  public readonly picture!: string;

  @Type(() => Account)
  public readonly account!: Account;

  @Type(() => Balance)
  public readonly balances!: Balance[];

  public readonly roles!: Role[];

  public getLdsFormattedBalance(): string {
    const ldsBalance = this.balances.find(balance => balance.isLDS());
    if(!ldsBalance) {
      return new FormattedAmount(BigInt(0)).format(9);
    }

    return ldsBalance.formattedBalance(9);
  }

  public getBnbFormattedBalance(): string {
    const ldsBalance = this.balances.find(balance => balance.isBNB());
    if(!ldsBalance) {
      return new FormattedAmount(BigInt(0)).format(9);
    }

    return ldsBalance.formattedBalance(9);
  }

  public getName(): string {
    return this.name || 'Unknown'
  }
}
