export class OfferUser {
  public readonly userId!: string;
  public readonly email!: string;
  public readonly name!: string;
  public readonly picture!: string;
  public readonly accountAddress!: string;
  public readonly accountId!: string;
  public readonly roles!: string[];

  public getName(): string {
    return this.name || 'Unknown'
  }
}
