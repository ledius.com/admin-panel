export enum LendingStatus {
  IN_PROGRESS,
  CLOSED,
  RETURNED,
}
