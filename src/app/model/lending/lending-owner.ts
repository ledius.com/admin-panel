import {Role} from "../role";

export class LendingOwner {
  public readonly id!: string;
  public readonly email!: string;
  public readonly name!: string;
  public readonly picture!: string;
  public readonly roles!: Role[];

  public getName(): string {
    return this.name || 'Unknown'
  }
}
