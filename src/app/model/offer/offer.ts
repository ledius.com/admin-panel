import {BankDetails} from "../bank-details/bank-details";
import {Transform, Type} from "class-transformer";
import {OfferStatus} from "./offer-status";
import {OfferType} from "./offer-type";
import {FormattedAmount} from "../formatted-amount";
import {OfferUser} from "../exchange/offer-user";

export class Offer {
  public readonly id!: string;

  @Transform(({value}) => {
    return BigInt(value);
  })
  public readonly amount!: bigint;

  public readonly rate!: { currency: string, price: number };
  public readonly status!: OfferStatus;
  public readonly type!: OfferType;

  @Type(() => OfferUser)
  public readonly owner!: OfferUser;

  @Type(() => OfferUser)
  public readonly reservedBy?: OfferUser;

  @Type(() => Date)
  public readonly reservedAt?: Date;

  @Type(() => BankDetails)
  public readonly bankDetails?: BankDetails;

  @Type(() => Date)
  public readonly publishedAt!: Date;

  @Type(() => Date)
  public readonly createdAt!: Date;

  @Type(() => Date)
  public readonly updatedAt!: Date;

  public get formattedAmount(): string {
    return new FormattedAmount(this.amount).format(9);
  }

  public get canBeClosed(): boolean {
    return this.status === OfferStatus.OPENED;
  }
}
