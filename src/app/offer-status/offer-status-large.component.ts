import { Component, Input } from '@angular/core';
import { OfferStatus } from "../model/offer/offer-status";

@Component({
  selector: 'app-offer-status-large',
  templateUrl: './offer-status-large.component.html',
  styleUrls: ['./offer-status-large.component.scss']
})
export class OfferStatusLargeComponent {

  @Input()
  public status!: OfferStatus;

  public get title(): string {
    return {
      [OfferStatus.OPENED]: 'Открыт',
      [OfferStatus.RESERVED]: 'Зарезервирован',
      [OfferStatus.FULFILLED]: 'Исполнен',
      [OfferStatus.CLOSED]: 'Закрыт'
    }[this.status];
  }

  public get icon(): string {
    return {
      [OfferStatus.OPENED]: '/assets/icons/open-icon.svg',
      [OfferStatus.RESERVED]: '/assets/icons/reserve-icon.svg',
      [OfferStatus.FULFILLED]: '/assets/icons/success-icon.svg',
      [OfferStatus.CLOSED]: '/assets/icons/close-icon.svg'
    }[this.status];
  }

  public get color(): string {
    return {
      [OfferStatus.OPENED]: '#007A00',
      [OfferStatus.RESERVED]: '#A75911',
      [OfferStatus.FULFILLED]: '#6751F0',
      [OfferStatus.CLOSED]: '#DB0000'
    }[this.status];
  }
}
