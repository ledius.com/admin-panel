import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferStatusLargeComponent } from './offer-status-large.component';

describe('OfferStatusLargeComponent', () => {
  let component: OfferStatusLargeComponent;
  let fixture: ComponentFixture<OfferStatusLargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferStatusLargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferStatusLargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
