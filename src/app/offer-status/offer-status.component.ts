import {Component, Input} from '@angular/core';
import {OfferStatus} from "../model/offer/offer-status";

@Component({
  selector: 'app-offer-status',
  templateUrl: './offer-status.component.html',
  styleUrls: ['./offer-status.component.scss']
})
export class OfferStatusComponent {

  @Input()
  public status!: OfferStatus;

  public get title(): string {
    return {
      [OfferStatus.OPENED]: 'Открыт',
      [OfferStatus.RESERVED]: 'Зарезервирован',
      [OfferStatus.FULFILLED]: 'Исполнен',
      [OfferStatus.CLOSED]: 'Закрыт'
    }[this.status] || 'Неизвестно';
  }

  public get color(): string {
    return {
      [OfferStatus.OPENED]: '#007A00',
      [OfferStatus.RESERVED]: '#A75911',
      [OfferStatus.FULFILLED]: '#6751F0',
      [OfferStatus.CLOSED]: '#DB0000'
    }[this.status] || '#000';
  }
}
