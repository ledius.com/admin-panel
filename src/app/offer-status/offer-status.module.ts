import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfferStatusComponent } from './offer-status.component';
import { OfferStatusLargeComponent } from './offer-status-large.component';



@NgModule({
  declarations: [
    OfferStatusComponent,
    OfferStatusLargeComponent
  ],
  exports: [
    OfferStatusComponent,
    OfferStatusLargeComponent
  ],
  imports: [
    CommonModule
  ]
})
export class OfferStatusModule { }
