import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {Course} from "../../model/learn/course";
import {plainToInstance} from "class-transformer";
import {UpdateCourseEvent} from "../../course-edit-form/course-edit-form.component";

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  public readonly $courses: BehaviorSubject<Course[]> = new BehaviorSubject<Course[]>([]);
  public readonly $course: BehaviorSubject<Course | null> = new BehaviorSubject<Course | null>(null);

  constructor(
    private readonly http: HttpClient
  ) {}

  public async fetchById(id: string): Promise<Course> {
    return await lastValueFrom(
      this.http.get<Course>(`/api/v1/courses/${id}`)
        .pipe(
          map((course) => plainToInstance(Course, course)),
          tap(course => this.$course.next(course))
        )
    );
  }

  public async fetchCourses(): Promise<Course[]> {
    return await lastValueFrom(
      this.http.get<{ items: Course[] }>('/api/v1/courses')
        .pipe(
          map(({items}) => plainToInstance(Course, items)),
          tap(courses => this.$courses.next(courses))
        )
    );
  }

  public async update(id: string, data: UpdateCourseEvent): Promise<Course> {
    return await lastValueFrom(
      this.http.put<Course>('/api/v1/courses', { id, ...data.content, ...data })
        .pipe(
          map((course) => plainToInstance(Course, course)),
          tap(course => this.$course.next(course))
        )
    );
  }
}
