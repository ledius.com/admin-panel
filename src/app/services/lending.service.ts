import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {Lending} from "../model/lending/lending";
import {plainToInstance} from "class-transformer";
import {Pagination} from "../model/pagination";

@Injectable({
  providedIn: 'root'
})
export class LendingService {

  public readonly $userLending: BehaviorSubject<Lending[]> = new BehaviorSubject<Lending[]>([]);
  public readonly $lendingList: BehaviorSubject<Lending[]> = new BehaviorSubject<Lending[]>([]);

  constructor(
    private readonly http: HttpClient,
  ) {}

  public async fetchUserLending(userId: string): Promise<Lending[]> {
    return await lastValueFrom(
      this.http.get<unknown[]>(`/api/ledius-token/lending/${userId}`)
        .pipe(
          map((lendingItems) => plainToInstance(Lending,  lendingItems)),
          tap(lendingItems => this.$userLending.next(lendingItems)),
        )
    )
  }

  public async returnLending(lending: Lending, userId: string): Promise<Lending> {
    return await lastValueFrom(
      this.http.patch(`/api/ledius-token/lending/${lending.id}/return`, {})
        .pipe(
          map((lendingItems) => plainToInstance(Lending,  lendingItems)),
          tap(() => this.fetchUserLending(userId))
        )
    )
  }

  public async create(payload: { userId: string, amount: string, endDate: Date } ): Promise<Lending> {
    return await lastValueFrom(
      this.http.post<Lending>('/api/ledius-token/lending', payload)
        .pipe(
          map(lending => plainToInstance(Lending, lending)),
          tap(() => this.fetchUserLending(payload.userId))
        )
    )
  }

  public async fetchLendingList(pagination: Pagination = Pagination.default()): Promise<Lending[]> {
    return await lastValueFrom(
      this.http.get<{ items: unknown[], pagination: Pagination }>(`/api/ledius-token/history/lending`)
        .pipe(
          map(({items}) => plainToInstance(Lending,  items)),
          tap(lendingItems => this.$lendingList.next(lendingItems)),
        )
    )
  }
}
