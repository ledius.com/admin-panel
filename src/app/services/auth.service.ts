import { Injectable } from '@angular/core';
import {GoogleLoginProvider, SocialAuthService, SocialUser} from "angularx-social-login";
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {AuthToken} from "../model/auth-token";
import {plainToInstance} from "class-transformer";
import {AuthData} from "../model/auth-data";
import {Role} from "../model/role";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public readonly $socialUser: BehaviorSubject<SocialUser | null> = new BehaviorSubject<SocialUser | null>(null);
  public readonly $authToken: BehaviorSubject<AuthToken | null> = new BehaviorSubject<AuthToken | null>(null);
  public readonly $authInfo: BehaviorSubject<AuthData | null> = new BehaviorSubject<AuthData | null>(null);


  constructor(
    private readonly socialAuthService: SocialAuthService,
    private readonly httpClient: HttpClient
  ) {
    this.socialAuthService.authState.subscribe(user => {
      this.$socialUser.next(user)
      if(!user) {
        this.$authInfo.next(null);
        this.$authToken.next(null);
      }
    });
    this.$socialUser.subscribe(async socialUser => {
      if(!socialUser) return;

      await this.authByIdToken(socialUser.idToken);
    });
    this.$authToken.subscribe(async authToken => {
      if(!authToken) return;

      await this.fetchAuthInfo(authToken);
    });
  }

  public async fetchAuthInfo(authToken: AuthToken): Promise<void> {
    await lastValueFrom(
      this.httpClient.get('/api/v1/auth/info')
        .pipe(
          map(info => plainToInstance(AuthData, info)),
          tap(data => this.$authInfo.next(data))
        )
    )
  }

  public async authByIdToken(idToken: string): Promise<void> {
    await lastValueFrom(
      this.httpClient.post<AuthToken>('/api/v1/auth/google-sign-in', { idToken })
        .pipe(
          map(token => plainToInstance(AuthToken, token)),
          tap(token => this.$authToken.next(token))
        )
    )
  }

  public async updateRoles(userId: string, roles: Role[]): Promise<void> {
    await lastValueFrom(
      this.httpClient.put('/api/v1/auth/update-roles', { userId, roles })
    )
  }

  public async logIn(): Promise<void> {
    await this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  public async logOut(): Promise<void> {
    await this.socialAuthService.signOut(true);
  }
}
