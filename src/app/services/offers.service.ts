import { Injectable } from '@angular/core';
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {Offer} from "../model/offer/offer";
import {HttpClient, HttpParams} from "@angular/common/http";
import {plainToInstance} from "class-transformer";
import {OfferType} from "../model/offer/offer-type";
import {OfferStatus} from "../model/offer/offer-status";
import {Pagination} from "../model/pagination";
import {BankDetails} from "../model/bank-details/bank-details";

@Injectable({
  providedIn: 'root'
})
export class OffersService {

  public readonly $offers: BehaviorSubject<Offer[]> = new BehaviorSubject<Offer[]>([]);
  public readonly $offer: BehaviorSubject<Offer | null> = new BehaviorSubject<Offer | null>(null);
  public readonly $pagination: BehaviorSubject<Pagination> = new BehaviorSubject<Pagination>(Pagination.default());

  constructor(
    private readonly http: HttpClient,
  ) {}

  public async payOffer(offer: Offer, reservedBy: string): Promise<{ paymentUrl: string }> {
    return await lastValueFrom(
      this.http.patch<{ paymentUrl: string }>('/api/ledius-token/offers/pay', { id: offer.id, reservedBy })
        .pipe(
          tap((result) => window.open(result.paymentUrl, '_blank'))
        )
    )
  }

  public async fetchOffers(paginationRequest?: Pagination): Promise<{ items: Offer[], pagination: Pagination }> {
    const { items, pagination } = await lastValueFrom(
      this.http.get<{ items: Offer[], pagination: Pagination }>(`/api/ledius-token/history/offers`, {
        params: new HttpParams({
          fromObject: {
            ...(paginationRequest ? paginationRequest.toObject() : this.$pagination.getValue().toObject()),
          },
        })
      })
        .pipe(
          map(({items, pagination}) => ({
            items: plainToInstance(Offer, items),
            pagination: plainToInstance(Pagination, pagination)
          }))
        )
    );

    this.$offers.next(items);
    this.$pagination.next(pagination);

    return { items, pagination };
  }

  public async fetchOfferById(id: string): Promise<Offer> {
    const offer = await lastValueFrom(this.http.get(`/api/ledius-token/history/offers/${id}`));
    const transformedOffer = plainToInstance(Offer, offer);
    await this.$offer.next(transformedOffer);

    return transformedOffer;
  }

  public async reserveOffer(offer: Offer, bankDetails: BankDetails): Promise<Offer> {
    const reservedOffer = await lastValueFrom(this.http.patch<Offer>('/api/ledius-token/offers/reserve', {
      id: offer.id,
      reservedBy: bankDetails.userId,
      bankDetails: bankDetails.id,
    }));

    return plainToInstance(Offer, reservedOffer);
  }
}
