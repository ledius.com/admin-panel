import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class OverlayService {

  public readonly $overlay: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {}

  public enable(): void {
    this.$overlay.next(true);
  }

  public off(): void {
    this.$overlay.next(false);
  }
}
