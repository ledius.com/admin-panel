import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../model/exchange/user";
import {BehaviorSubject, lastValueFrom, map, tap} from "rxjs";
import {plainToInstance} from "class-transformer";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public readonly $users: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  public readonly $user: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);

  constructor(
    private readonly http: HttpClient
  ) {}

  public async fetchUsers(): Promise<User[]> {
    return await lastValueFrom(
      this.http.get<User[]>('/api/ledius-token/users')
        .pipe(
          map(users => plainToInstance(User, users)),
          tap(users => this.$users.next(users))
        )
    )
  }

  public async fetchUserById(userId: string): Promise<User> {
    return await lastValueFrom(
      this.http.get<User>(`/api/ledius-token/users/${userId}`)
        .pipe(
          map(user => plainToInstance(User, user)),
          tap(user => this.$user.next(user))
        )
    )
  }
}
