import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../model/exchange/user";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent {

  @Input()
  public users: User[] = [];

  @Output()
  public readonly forward: EventEmitter<User> = new EventEmitter<User>();

  public async onForward(user: User): Promise<void> {
    await this.forward.emit(user);
  }
}
