import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list.component';
import { UsersListItemComponent } from './users-list-item/users-list-item.component';
import {UserRoleBadgeModule} from "../user-role-badge/user-role-badge.module";



@NgModule({
    declarations: [
        UsersListComponent,
        UsersListItemComponent
    ],
    exports: [
        UsersListComponent
    ],
  imports: [
    CommonModule,
    UserRoleBadgeModule
  ]
})
export class UsersListModule { }
