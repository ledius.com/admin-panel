import {Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../../model/exchange/user";

@Component({
  selector: 'app-users-list-item',
  templateUrl: './users-list-item.component.html',
  styleUrls: ['./users-list-item.component.scss']
})
export class UsersListItemComponent {

  @Input()
  public user!: User;

  @Output()
  public readonly forward: EventEmitter<User> = new EventEmitter<User>();

  public async clickForward(): Promise<void> {
    this.forward.emit(this.user);
  }

  public handleMissingAvatar(event: Event) : void {
    (event.target as HTMLImageElement).src = '/assets/icons/avatar.png'
  }

}
