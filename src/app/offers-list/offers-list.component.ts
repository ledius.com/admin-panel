import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Offer} from "../model/offer/offer";

@Component({
  selector: 'app-offers-list',
  templateUrl: './offers-list.component.html',
  styleUrls: ['./offers-list.component.scss']
})
export class OffersListComponent {
  @Input()
  public offers: Offer[] = [];

  @Output()
  public forward: EventEmitter<Offer> = new EventEmitter<Offer>();

  public onForward(offer: Offer) {
    this.forward.emit(offer);
  }
}
