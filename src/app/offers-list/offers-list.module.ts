import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersListComponent } from './offers-list.component';
import { OffersListItemComponent } from './offers-list-item/offers-list-item.component';
import {OfferStatusModule} from "../offer-status/offer-status.module";
import {OfferTypeModule} from "../offer-type/offer-type.module";



@NgModule({
    declarations: [
        OffersListComponent,
        OffersListItemComponent
    ],
    exports: [
        OffersListComponent
    ],
  imports: [
    CommonModule,
    OfferStatusModule,
    OfferTypeModule
  ]
})
export class OffersListModule { }
