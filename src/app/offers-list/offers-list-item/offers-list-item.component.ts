import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Offer} from "../../model/offer/offer";

@Component({
  selector: 'app-offers-list-item',
  templateUrl: './offers-list-item.component.html',
  styleUrls: ['./offers-list-item.component.scss']
})
export class OffersListItemComponent {

  @Input()
  public offer!: Offer;

  @Output()
  public forward: EventEmitter<Offer> = new EventEmitter<Offer>();

  public open() {
    this.forward.emit(this.offer);
  }
}
