import {Component, Input} from '@angular/core';
import {OfferUser} from "../model/exchange/offer-user";
import {User} from "../model/exchange/user";
import {LendingOwner} from "../model/lending/lending-owner";

@Component({
  selector: 'app-user-preview-card',
  templateUrl: './user-preview-card.component.html',
  styleUrls: ['./user-preview-card.component.scss']
})
export class UserPreviewCardComponent {
  @Input()
  public userId?: string;

  @Input()
  public user!: OfferUser | User | LendingOwner;

  public get id(): string {
    const user = this.user;
    if(this.userId) {
      return this.userId;
    }

    if((user).hasOwnProperty('id')) {
      return (user as LendingOwner).id;
    }

    return (user as User).userId;
  }
}
