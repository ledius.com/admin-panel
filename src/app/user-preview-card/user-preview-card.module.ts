import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPreviewCardComponent } from './user-preview-card.component';
import { ReservedImageDirective } from "../directives/reserved-image.directive";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    UserPreviewCardComponent,
    ReservedImageDirective,
  ],
  exports: [
    UserPreviewCardComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class UserPreviewCardModule { }
