import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionListComponent } from './transaction-list.component';
import { TransactionCardModule } from "../transaction-card/transaction-card.module";

@NgModule({
  declarations: [
    TransactionListComponent
  ],
  exports: [
    TransactionListComponent
  ],
  imports: [
    CommonModule,
    TransactionCardModule
  ]
})
export class TransactionListModule { }
