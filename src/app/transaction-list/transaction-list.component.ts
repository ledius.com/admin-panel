import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit {

  @Input()
  public transactions: {id: string}[] = [
    { id: '999099999' },
    { id: '999099991' },
    { id: '9990999912' },
  ];

  @Output()
  public openTransaction: EventEmitter<{ id: string }> = new EventEmitter<{id: string}>();

  constructor() { }

  ngOnInit(): void {
  }

  public onOpenTransaction(transaction: {id: string}) {
    this.openTransaction.emit(transaction);
  }

}
