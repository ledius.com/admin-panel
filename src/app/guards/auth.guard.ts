import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { AuthService } from "../services/auth.service";
import {
  catchError,
  concatMap,
  delay,
  delayWhen,
  map,
  mergeMap,
  Observable,
  of,
  retry,
  retryWhen,
  throwError
} from "rxjs";
import {OverlayService} from "../services/overlay.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private readonly authService: AuthService,
    private readonly overlayService: OverlayService
  ) {}

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authService.$authInfo.pipe(
      delay(10),
      mergeMap((info) => {
        if(!info) {
          this.overlayService.enable();
          return throwError(() => new Error('unauthorized'))
        }

        this.overlayService.off();
        return of(true);
      }),
      retry(1000),
      catchError(() => of(false))
    );
  }
}
