import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionDetailsComponent } from './transaction-details.component';



@NgModule({
    declarations: [
        TransactionDetailsComponent
    ],
    exports: [
        TransactionDetailsComponent
    ],
    imports: [
        CommonModule
    ]
})
export class TransactionDetailsModule { }
