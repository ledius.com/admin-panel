import { Component } from '@angular/core';
import {Location} from "@angular/common";
import {AuthService} from "./services/auth.service";
import {OverlayService} from "./services/overlay.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'admin';
  public menuIsOpen: boolean = false;
  public isAuth: boolean = false;
  public overlay: boolean = false;


  constructor(
    private readonly location: Location,
    private readonly authService: AuthService,
    private readonly overlayService: OverlayService,
  ) {
    this.authService.$socialUser.subscribe(user => {
      if(user) this.isAuth = true
    });
    this.overlayService.$overlay.subscribe(overlay => this.overlay = overlay);
  }

  public async onBackward(): Promise<void> {
    this.location.back();
  }

  public async onLogin(): Promise<void> {
    await this.authService.logIn();
  }
}
