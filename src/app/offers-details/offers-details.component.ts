import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Offer} from "../model/offer/offer";

@Component({
  selector: 'app-offers-details',
  templateUrl: './offers-details.component.html',
  styleUrls: ['./offers-details.component.scss']
})
export class OffersDetailsComponent {

  @Input()
  public offer!: Offer;

  @Output()
  public close: EventEmitter<Offer> = new EventEmitter<Offer>();

  public closeOffer(): void {
    this.close.emit(this.offer);
  }
}
