import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersDetailsComponent } from './offers-details.component';
import {UserPreviewCardModule} from "../user-preview-card/user-preview-card.module";
import {BankDetailsModule} from "../bank-details/bank-details.module";
import {OfferStatusModule} from "../offer-status/offer-status.module";
import {OfferTypeModule} from "../offer-type/offer-type.module";



@NgModule({
    declarations: [
        OffersDetailsComponent
    ],
    exports: [
        OffersDetailsComponent
    ],
  imports: [
    CommonModule,
    UserPreviewCardModule,
    BankDetailsModule,
    OfferStatusModule,
    OfferTypeModule
  ]
})
export class OffersDetailsModule { }
