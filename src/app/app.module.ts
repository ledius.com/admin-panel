import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {HeaderModule} from "./header/header.module";
import {MenuModule} from "./menu/menu.module";
import { AppRoutingModule } from './app-routing.module';
import {TransactionsViewModule} from "./views/transactions-view/transactions-view.module";
import {UsersViewModule} from "./views/users-view/users-view.module";
import { UsersDetailsViewModule } from "./views/users-details-view/users-details-view.module";
import {OffersViewModule} from "./views/offers-view/offers-view.module";
import {OffersDetailedViewModule} from "./views/offers-detailed-view/offers-detailed-view.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule} from "angularx-social-login";
import {environment} from "../environments/environment";
import {AuthInterceptor} from "./interceptors/auth.interceptor";
import {BalancesViewModule} from "./views/balances-view/balances-view.module";
import {LendingViewModule} from "./views/lending-view/lending-view.module";
import {CoursesViewModule} from "./views/courses-view/courses-view.module";
import {CourseEditViewModule} from "./views/course-edit-view/course-edit-view.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HeaderModule,
    MenuModule,
    AppRoutingModule,
    TransactionsViewModule,
    UsersViewModule,
    UsersDetailsViewModule,
    OffersViewModule,
    OffersDetailedViewModule,
    HttpClientModule,
    SocialLoginModule,
    BalancesViewModule,
    LendingViewModule,
    CoursesViewModule,
    CourseEditViewModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: true,
        providers: [
          {
            provider: new GoogleLoginProvider(environment.googleAuthClientId),
            id: GoogleLoginProvider.PROVIDER_ID
          }
        ]
      } as SocialAuthServiceConfig
    }
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
