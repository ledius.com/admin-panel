import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {TransactionsViewComponent} from "./views/transactions-view/transactions-view.component";
import {TransactionDetailsViewComponent} from "./views/transactions-view/transaction-details-view.component";
import {UsersViewComponent} from "./views/users-view/users-view.component";
import {UsersDetailsViewComponent} from "./views/users-details-view/users-details-view.component";
import {OffersViewComponent} from "./views/offers-view/offers-view.component";
import {OffersDetailedViewComponent} from "./views/offers-detailed-view/offers-detailed-view.component";
import {UsersViewResolver} from "./resolvers/users-view.resolver";
import {UserDetailsViewResolver} from "./resolvers/user-details-view.resolver";
import {OffersViewResolver} from "./resolvers/offers-view.resolver";
import {OfferDetailsViewResolver} from "./resolvers/offer-details-view.resolver";
import {AuthGuard} from "./guards/auth.guard";
import {BalancesViewComponent} from "./views/balances-view/balances-view.component";
import {AccountBalancesResolver} from "./resolvers/account-balances.resolver";
import {LendingViewComponent} from "./views/lending-view/lending-view.component";
import {LendingViewResolver} from "./resolvers/lending-view.resolver";
import {CoursesViewComponent} from "./views/courses-view/courses-view.component";
import {CoursesViewResolver} from "./resolvers/courses-view.resolver";
import {CourseEditViewComponent} from "./views/course-edit-view/course-edit-view.component";
import {CourseEditViewResolver} from "./resolvers/course-edit-view.resolver";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'users',
  },
  {
    path: 'transactions',
    component: TransactionsViewComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'transactions/:transactionId/details',
    component: TransactionDetailsViewComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'users',
    component: UsersViewComponent,
    resolve: [UsersViewResolver],
    canActivate: [AuthGuard],
  },
  {
    path: 'users/:userId/details',
    component: UsersDetailsViewComponent,
    resolve: [UserDetailsViewResolver],
    canActivate: [AuthGuard],
  },
  {
    path: 'offers',
    component: OffersViewComponent,
    resolve: [OffersViewResolver],
    canActivate: [AuthGuard],
  },
  {
    path: 'offers/:offerId/details',
    component: OffersDetailedViewComponent,
    resolve: [OfferDetailsViewResolver],
    canActivate: [AuthGuard],
  },
  {
    path: 'balances',
    component: BalancesViewComponent,
    resolve: [AccountBalancesResolver],
    canActivate: [AuthGuard]
  },
  {
    path: 'lending',
    component: LendingViewComponent,
    resolve: [LendingViewResolver],
    canActivate: [AuthGuard]
  },
  {
    path: 'courses',
    component: CoursesViewComponent,
    resolve: [CoursesViewResolver],
    canActivate: [AuthGuard]
  },
  {
    path: 'courses/:courseId/edit',
    component: CourseEditViewComponent,
    resolve: [CourseEditViewResolver],
    canActivate: [AuthGuard]
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
