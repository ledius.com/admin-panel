import { TestBed } from '@angular/core/testing';

import { OfferDetailsViewResolver } from './offer-details-view.resolver';

describe('OfferDetailsViewResolver', () => {
  let resolver: OfferDetailsViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(OfferDetailsViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
