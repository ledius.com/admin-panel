import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { of } from 'rxjs';
import {OffersService} from "../services/offers.service";
import {Pagination} from "../model/pagination";
import {OverlayService} from "../services/overlay.service";

@Injectable({
  providedIn: 'root'
})
export class OffersViewResolver implements Resolve<void> {

  constructor(
    private readonly offersService: OffersService,
    private readonly overlay: OverlayService
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    await this.overlay.enable();
    await this.offersService.fetchOffers(new Pagination(0, 1000));
    await this.overlay.off();
  }
}
