import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {UsersService} from "../services/exchange/users.service";
import {OverlayService} from "../services/overlay.service";

@Injectable({
  providedIn: 'root'
})
export class UserDetailsViewResolver implements Resolve<void> {

  constructor(
    private readonly usersService: UsersService,
    private readonly overlay: OverlayService
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    await this.overlay.enable();
    await this.usersService.fetchUserById(route.params['userId']);
    await this.overlay.off();
  }
}
