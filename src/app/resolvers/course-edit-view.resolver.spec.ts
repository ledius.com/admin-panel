import { TestBed } from '@angular/core/testing';

import { CourseEditViewResolver } from './course-edit-view.resolver';

describe('CourseEditViewResolver', () => {
  let resolver: CourseEditViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(CourseEditViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
