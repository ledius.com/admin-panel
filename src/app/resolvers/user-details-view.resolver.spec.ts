import { TestBed } from '@angular/core/testing';

import { UserDetailsViewResolver } from './user-details-view.resolver';

describe('UserDetailsViewResolver', () => {
  let resolver: UserDetailsViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(UserDetailsViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
