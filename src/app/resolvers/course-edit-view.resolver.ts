import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {CoursesService} from "../services/learn/courses.service";
import {OverlayService} from "../services/overlay.service";

@Injectable({
  providedIn: 'root'
})
export class CourseEditViewResolver implements Resolve<void> {

  constructor(
    private readonly coursesService: CoursesService,
    private readonly overlay: OverlayService,
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    this.overlay.enable();
    await this.coursesService.fetchById(route.params['courseId']);
    this.overlay.off();
  }
}
