import { TestBed } from '@angular/core/testing';

import { CoursesViewResolver } from './courses-view.resolver';

describe('CoursesViewResolver', () => {
  let resolver: CoursesViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(CoursesViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
