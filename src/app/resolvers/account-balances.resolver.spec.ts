import { TestBed } from '@angular/core/testing';

import { AccountBalancesResolver } from './account-balances-resolver.resolver';

describe('AccountBalancesResolverResolver', () => {
  let resolver: AccountBalancesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(AccountBalancesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
