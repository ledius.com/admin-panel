import { TestBed } from '@angular/core/testing';

import { UsersViewResolver } from './users-view.resolver';

describe('UsersViewResolver', () => {
  let resolver: UsersViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(UsersViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
