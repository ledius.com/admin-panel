import { TestBed } from '@angular/core/testing';

import { LendingViewResolver } from './lending-view.resolver';

describe('LendingViewResolver', () => {
  let resolver: LendingViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(LendingViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
