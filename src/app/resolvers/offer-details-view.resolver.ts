import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {OffersService} from "../services/offers.service";
import {OverlayService} from "../services/overlay.service";

@Injectable({
  providedIn: 'root'
})
export class OfferDetailsViewResolver implements Resolve<void> {

  constructor(
    private readonly offersService: OffersService,
    private readonly overlay: OverlayService
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    await this.overlay.enable();
    await this.offersService.fetchOfferById(route.params['offerId']);
    await this.overlay.off();
  }
}
