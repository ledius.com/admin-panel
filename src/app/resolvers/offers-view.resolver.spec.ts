import { TestBed } from '@angular/core/testing';

import { OffersViewResolver } from './offers-view.resolver';

describe('OffersViewResolver', () => {
  let resolver: OffersViewResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(OffersViewResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
