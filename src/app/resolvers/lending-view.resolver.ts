import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {OverlayService} from "../services/overlay.service";
import {LendingService} from "../services/lending.service";
import {Pagination} from "../model/pagination";

@Injectable({
  providedIn: 'root'
})
export class LendingViewResolver implements Resolve<void> {

  constructor(
    private readonly overlay: OverlayService,
    private readonly lendingService: LendingService
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    await this.overlay.enable();
    await this.lendingService.fetchLendingList(new Pagination(0, 1000));
    await this.overlay.off();
  }
}
