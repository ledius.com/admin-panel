import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { OverlayService } from "../services/overlay.service";
import { CoursesService } from "../services/learn/courses.service";

@Injectable({
  providedIn: 'root'
})
export class CoursesViewResolver implements Resolve<void> {

  constructor(
    private readonly overlay: OverlayService,
    private readonly coursesService: CoursesService,
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    this.overlay.enable();
    await this.coursesService.fetchCourses();
    this.overlay.off();
  }
}
