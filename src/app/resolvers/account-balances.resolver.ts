import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {AccountService} from "../services/account.service";
import {OverlayService} from "../services/overlay.service";

@Injectable({
  providedIn: 'root'
})
export class AccountBalancesResolver implements Resolve<void> {
  constructor(
    private readonly accountService: AccountService,
    private readonly overlay: OverlayService,
  ) {}

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    await this.overlay.enable();
    await this.accountService.fetchSystemBalances();
    await this.overlay.off();
  }
}
