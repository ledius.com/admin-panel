import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {UsersService} from "../services/exchange/users.service";
import {OverlayService} from "../services/overlay.service";

@Injectable({
  providedIn: 'root'
})
export class UsersViewResolver implements Resolve<void> {

  constructor(
    private readonly usersService: UsersService,
    private readonly overlay: OverlayService
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<void> {
    await this.overlay.enable();
    await this.usersService.fetchUsers();
    await this.overlay.off();
  }
}
