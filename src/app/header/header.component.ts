import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Output()
  public toggleMenu: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  public backward: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  public login: EventEmitter<void> = new EventEmitter<void>();

  @Input()
  public isAuth: boolean = false;

  constructor() { }

  public menuToggle() {
    this.toggleMenu.emit();
  }

  public clickBackward() {
    this.backward.emit();
  }

  public loginClick() {
    this.login.emit();
  }
}
