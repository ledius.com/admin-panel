import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../model/exchange/user";
import {Role} from "../model/role";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent {

  @Input()
  public user!: User;

  @Output()
  public updateRoles: EventEmitter<Role[]> = new EventEmitter<Role[]>();

  public handleMissingAvatar(event: Event): void {
    (event.target as HTMLImageElement).src = '/assets/icons/avatar.png';
  }

  public onUpdatedRoles(roles: Role[]): void {
    this.updateRoles.emit(roles);
  }
}
