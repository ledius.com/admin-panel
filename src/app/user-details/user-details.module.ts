import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailsComponent } from './user-details.component';
import {SwitchModule} from "../components/switch/switch.module";
import {UserRoleBadgeModule} from "../user-role-badge/user-role-badge.module";
import {RoleManageModule} from "../role-manage/role-manage.module";



@NgModule({
    declarations: [
        UserDetailsComponent
    ],
    exports: [
        UserDetailsComponent
    ],
  imports: [
    CommonModule,
    SwitchModule,
    UserRoleBadgeModule,
    RoleManageModule
  ]
})
export class UserDetailsModule { }
