import {Component, Input, OnInit} from '@angular/core';
import {Account} from "../model/account/account";
import {Balance} from "../model/account/balance";

export type AccountWithBalances = {
  account: Account;
  name: string;
  balances: Balance[];
}

@Component({
  selector: 'app-account-balance-list',
  templateUrl: './account-balance-list.component.html',
  styleUrls: ['./account-balance-list.component.scss']
})
export class AccountBalanceListComponent {
  @Input()
  public accounts!: AccountWithBalances[];
}
