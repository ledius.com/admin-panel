import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountBalanceListComponent } from './account-balance-list.component';
import { AccountBalanceListItemComponent } from './account-balance-list-item/account-balance-list-item.component';
import {CardModule} from "../components/card/card.module";



@NgModule({
  declarations: [
    AccountBalanceListComponent,
    AccountBalanceListItemComponent
  ],
  exports: [
    AccountBalanceListComponent
  ],
  imports: [
    CommonModule,
    CardModule
  ]
})
export class AccountBalanceListModule { }
