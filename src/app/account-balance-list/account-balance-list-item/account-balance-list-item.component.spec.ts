import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountBalanceListItemComponent } from './account-balance-list-item.component';

describe('AccountBalanceListItemComponent', () => {
  let component: AccountBalanceListItemComponent;
  let fixture: ComponentFixture<AccountBalanceListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountBalanceListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountBalanceListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
