import {Component, Input, OnInit} from '@angular/core';
import {Account} from "../../model/account/account";
import {Balance} from "../../model/account/balance";

@Component({
  selector: 'app-account-balance-list-item',
  templateUrl: './account-balance-list-item.component.html',
  styleUrls: ['./account-balance-list-item.component.scss']
})
export class AccountBalanceListItemComponent {

  @Input()
  public account!: Account;

  @Input()
  public balances: Balance[] = [];

  @Input()
  public name: string = '';

  public getLdsBalance(): Balance | undefined {
    return this.balances.find(balance => balance.isLDS());
  }

  public getBnbBalance(): Balance | undefined {
    return this.balances.find(balance => balance.isBNB());
  }

}
