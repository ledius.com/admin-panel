import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfferTypeComponent } from './offer-type.component';
import { OfferTypeLargeComponent } from './offer-type-large.component';



@NgModule({
  declarations: [
    OfferTypeComponent,
    OfferTypeLargeComponent
  ],
  exports: [
    OfferTypeComponent,
    OfferTypeLargeComponent
  ],
  imports: [
    CommonModule
  ]
})
export class OfferTypeModule { }
