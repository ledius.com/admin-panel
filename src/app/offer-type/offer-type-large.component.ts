import {Component, Input} from '@angular/core';
import {OfferType} from "../model/offer/offer-type";

@Component({
  selector: 'app-offer-type-large',
  templateUrl: './offer-type-large.component.html',
  styleUrls: ['./offer-type-large.component.scss']
})
export class OfferTypeLargeComponent {

  @Input()
  public type!: OfferType;

  public get title(): string {
    return {
      [OfferType.BUY]: 'Покупка',
      [OfferType.SELL]: 'Продажа'
    }[this.type];
  }

  public get icon(): string {
    return {
      [OfferType.BUY]: '/assets/icons/buy-coin-icon.svg',
      [OfferType.SELL]: '/assets/icons/sell-coin-icon.svg'
    }[this.type];
  }

  public get color(): string {
    return {
      [OfferType.BUY]: '#007A00',
      [OfferType.SELL]: '#DB0000'
    }[this.type];
  }
}
