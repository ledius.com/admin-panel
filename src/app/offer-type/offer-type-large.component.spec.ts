import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferTypeLargeComponent } from './offer-type-large.component';

describe('OfferTypeLargeComponent', () => {
  let component: OfferTypeLargeComponent;
  let fixture: ComponentFixture<OfferTypeLargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferTypeLargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferTypeLargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
