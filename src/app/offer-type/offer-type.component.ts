import {Component, Input } from '@angular/core';
import {OfferType} from "../model/offer/offer-type";

@Component({
  selector: 'app-offer-type',
  templateUrl: './offer-type.component.html',
  styleUrls: ['./offer-type.component.scss']
})
export class OfferTypeComponent {

  @Input()
  public type!: OfferType;

  public get title(): string {
    return {
      [OfferType.BUY]: 'Покупка',
      [OfferType.SELL]: 'Продажа',
    }[this.type] || 'Неизвестно';
  }

  public get color(): string {
    return {
      [OfferType.BUY]: '#007A00',
      [OfferType.SELL]: '#DB0000',
    }[this.type] || '#000';
  }

}
