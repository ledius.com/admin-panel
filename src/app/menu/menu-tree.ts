export interface MenuTree {
  readonly icon: string;
  readonly title: string;
  readonly child: MenuTree[];
  readonly route?: string;
}
