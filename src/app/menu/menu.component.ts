import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MenuTree} from "./menu-tree";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public tree: MenuTree[] = [
    {
      icon: 'assets/icons/exchange-icon.svg',
      title: 'Ledius Exchange',
      child: [
        {
          icon: 'assets/icons/offer-icon.svg',
          title: 'Сделки',
          route: '/offers',
          child: [],
        },
        {
          icon: 'assets/icons/transactions-icon.svg',
          title: 'Транзакции',
          child: [],
          route: '/transactions'
        },
        {
          icon: 'assets/icons/users-icon.svg',
          title: 'Пользователи',
          child: [],
          route: '/users'
        },
        {
          icon: 'assets/icons/users-icon.svg',
          title: 'Балансы',
          child: [],
          route: '/balances'
        },
        {
          icon: 'assets/icons/users-icon.svg',
          title: 'Депозиты',
          child: [],
          route: '/lending'
        }
      ],
    },
    {
      icon: 'assets/icons/business-icon.svg',
      child: [],
      title: 'Ledius Business'
    },
    {
      icon: 'assets/icons/learn-icon.svg',
      title: 'Ledius Learn',
      child: [
        {
          icon: '',
          route: '/courses',
          title: 'Курсы',
          child: []
        }
      ],
    },
    {
      icon: 'assets/icons/bsc-icon.svg',
      child: [],
      title: 'Ledius BscScan'
    },
  ]

  @Output()
  public readonly close: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  public emitClose(): void {
    this.close.emit();
  }
}
