import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankDetailsCardComponent } from './bank-details-card/bank-details-card.component';



@NgModule({
  declarations: [
    BankDetailsCardComponent,
    BankDetailsCardComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    BankDetailsCardComponent,
  ]
})
export class BankDetailsModule { }
