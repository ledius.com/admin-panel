import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankDetailsCardComponent } from './bank-details-card.component';

describe('BankDetailsCardComponent', () => {
  let component: BankDetailsCardComponent;
  let fixture: ComponentFixture<BankDetailsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankDetailsCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankDetailsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
