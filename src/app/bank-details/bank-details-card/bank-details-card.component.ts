import { Component, Input } from '@angular/core';
import { BankDetails } from "../../model/bank-details/bank-details";

@Component({
  selector: 'app-bank-details-card[bankDetails]',
  templateUrl: './bank-details-card.component.html',
  styleUrls: ['./bank-details-card.component.scss']
})
export class BankDetailsCardComponent {

  @Input()
  public bankDetails!: BankDetails;

}
