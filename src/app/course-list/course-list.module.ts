import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseListComponent } from './course-list.component';
import { CourseListItemComponent } from './course-list-item/course-list-item.component';
import {CardModule} from "../components/card/card.module";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    CourseListComponent,
    CourseListItemComponent
  ],
  exports: [
    CourseListComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    RouterModule
  ]
})
export class CourseListModule { }
