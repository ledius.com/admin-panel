import { Component, Input } from '@angular/core';
import { Course } from "../model/learn/course";

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent {
  @Input()
  public courses: Course[] = [];
}
