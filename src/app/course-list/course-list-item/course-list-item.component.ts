import {Component, Input} from '@angular/core';
import {Course} from "../../model/learn/course";

@Component({
  selector: 'app-course-list-item',
  templateUrl: './course-list-item.component.html',
  styleUrls: ['./course-list-item.component.scss']
})
export class CourseListItemComponent {

  @Input()
  public course!: Course;

}
