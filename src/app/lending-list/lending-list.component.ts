import { Component, Input } from '@angular/core';
import { Lending } from "../model/lending/lending";

@Component({
  selector: 'app-lending-list',
  templateUrl: './lending-list.component.html',
  styleUrls: ['./lending-list.component.scss']
})
export class LendingListComponent {

  @Input()
  public lendingList: Lending[] = [];

}
