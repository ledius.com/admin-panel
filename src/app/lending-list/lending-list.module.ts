import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LendingListComponent } from './lending-list.component';
import { LendingListItemComponent } from './lending-list-item/lending-list-item.component';
import {CardModule} from "../components/card/card.module";
import {UserPreviewCardModule} from "../user-preview-card/user-preview-card.module";



@NgModule({
  declarations: [
    LendingListComponent,
    LendingListItemComponent
  ],
  exports: [
    LendingListComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    UserPreviewCardModule
  ]
})
export class LendingListModule { }
