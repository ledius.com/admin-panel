import { Component, Input } from '@angular/core';
import { Lending } from "../../model/lending/lending";

@Component({
  selector: 'app-lending-list-item',
  templateUrl: './lending-list-item.component.html',
  styleUrls: ['./lending-list-item.component.scss']
})
export class LendingListItemComponent {

  @Input()
  public lending!: Lending;

}
