import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LendingListItemComponent } from './lending-list-item.component';

describe('LendingListItemComponent', () => {
  let component: LendingListItemComponent;
  let fixture: ComponentFixture<LendingListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LendingListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LendingListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
