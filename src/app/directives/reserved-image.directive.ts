import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[appReservedImage]'
})
export class ReservedImageDirective {

  @Input()
  public appReservedImage: string = 'assets/icons/avatar.png';

  constructor(private readonly el: ElementRef) {
    const imageElement = el.nativeElement as HTMLImageElement;
    imageElement.onerror = () => imageElement.src = this.appReservedImage;
  }

}
