import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionCardComponent } from './transaction-card.component';



@NgModule({
    declarations: [
        TransactionCardComponent
    ],
    exports: [
        TransactionCardComponent
    ],
    imports: [
        CommonModule,
    ]
})
export class TransactionCardModule { }
