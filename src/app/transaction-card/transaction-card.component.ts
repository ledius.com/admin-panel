import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-transaction-card',
  templateUrl: './transaction-card.component.html',
  styleUrls: ['./transaction-card.component.scss']
})
export class TransactionCardComponent implements OnInit {

  @Output()
  public readonly openTransaction: EventEmitter<{ id: string }> = new EventEmitter<{ id: string }>();

  @Input()
  public transaction!: { id: string };

  constructor() { }

  public open(): void {
    this.openTransaction.emit(this.transaction);
  }

  ngOnInit(): void {
  }
}
