import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseEditFormComponent } from './course-edit-form.component';
import {CardModule} from "../components/card/card.module";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    CourseEditFormComponent
  ],
  exports: [
    CourseEditFormComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    ReactiveFormsModule
  ]
})
export class CourseEditFormModule { }
