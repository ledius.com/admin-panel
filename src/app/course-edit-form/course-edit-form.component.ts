import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Course } from "../model/learn/course";
import {FormArray, FormControl, FormGroup} from "@angular/forms";

export type UpdateCourseEvent = {
  content: {
    name: string,
    icon: string,
  },
  price: number,
  chips: {
    text: string,
    icon: string
  }[]
};

@Component({
  selector: 'app-course-edit-form[course]',
  templateUrl: './course-edit-form.component.html',
  styleUrls: ['./course-edit-form.component.scss']
})
export class CourseEditFormComponent implements OnInit {

  @Input()
  public course!: Course;

  @Output()
  public update: EventEmitter<UpdateCourseEvent>
    = new EventEmitter<UpdateCourseEvent>();

  public courseForm = new FormGroup({
    name: new FormControl(''),
    icon: new FormControl(''),
    price: new FormControl(''),
    chips: new FormArray([])
  });

  get chips() {
    return this.courseForm.get('chips') as FormArray;
  }

  public ngOnInit(): void {
    if(!this.course) {
      return;
    }

    this.courseForm.get('name')?.setValue(this.course.content.name);
    this.courseForm.get('price')?.setValue(this.course.getRubPrice());
    this.courseForm.get('icon')?.setValue(this.course.content.icon);
    this.course.chips.map((chip, index) => {
      (this.courseForm.get('chips') as FormArray).setControl(index, new FormControl(chip.text));
    });
  }

  public removeChip(index: number): void {
    this.chips.removeAt(index);
  }

  public addChip(): void {
    this.chips.push(new FormControl(''));
  }

  public save(): void {
    this.update.emit({
      content: {
        name: this.courseForm.get('name')?.value ?? '',
        icon: this.courseForm.get('icon')?.value ?? '',
      },
      price: this.courseForm.get('price')?.value ?? '',
      chips: this.chips.getRawValue().map(chip => ({text: chip, icon: ''}))
    });
  }

}
